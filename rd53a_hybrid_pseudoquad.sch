EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:74xx
LIBS:audio
LIBS:interface
LIBS:connsingle
LIBS:rd53a_hybrid_wirebonds
LIBS:solder_pad
LIBS:rd53a_hybrid_pseudoquad-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 9
Title "RD53A hybrid pseudoquad"
Date ""
Rev "2.0"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 1400 850  1850 800 
U 5B1BB7BD
F0 "Chip_A" 60
F1 "SingleChip.sch" 60
F2 "VIN_RAW" I L 1400 1200 60 
F3 "VIN_RET" O R 3250 1200 60 
$EndSheet
Wire Wire Line
	1400 1200 900  1200
Text Label 900  1200 0    60   ~ 0
VIN_RAW
Wire Wire Line
	3250 1200 3700 1200
Text Label 3700 1200 2    60   ~ 0
VIN_RET
$Sheet
S 1400 2150 1850 800 
U 5B1B2A29
F0 "Chip_B" 60
F1 "SingleChip.sch" 60
F2 "VIN_RAW" I L 1400 2500 60 
F3 "VIN_RET" O R 3250 2500 60 
$EndSheet
Wire Wire Line
	1400 2500 900  2500
Text Label 900  2500 0    60   ~ 0
VIN_RAW
Wire Wire Line
	3250 2500 3700 2500
Text Label 3700 2500 2    60   ~ 0
VIN_RET
$Sheet
S 1400 3400 1850 800 
U 5B1B2A32
F0 "Chip_C" 60
F1 "SingleChip.sch" 60
F2 "VIN_RAW" I L 1400 3750 60 
F3 "VIN_RET" O R 3250 3750 60 
$EndSheet
Wire Wire Line
	1400 3750 900  3750
Text Label 900  3750 0    60   ~ 0
VIN_RAW
Wire Wire Line
	3250 3750 3700 3750
Text Label 3700 3750 2    60   ~ 0
VIN_RET
$Sheet
S 1400 4700 1850 800 
U 5B1B2A3B
F0 "Chip_D" 60
F1 "SingleChip.sch" 60
F2 "VIN_RAW" I L 1400 5050 60 
F3 "VIN_RET" O R 3250 5050 60 
$EndSheet
Wire Wire Line
	1400 5050 900  5050
Text Label 900  5050 0    60   ~ 0
VIN_RAW
Wire Wire Line
	3250 5050 3700 5050
Text Label 3700 5050 2    60   ~ 0
VIN_RET
$EndSCHEMATC
